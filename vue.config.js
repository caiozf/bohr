const path = require('path');
const PrerenderSPAPlugin = require('prerender-spa-plugin');

module.exports = {
  configureWebpack: {
    plugins: [
      new PrerenderSPAPlugin({
        staticDir: path.join(__dirname, 'dist'),
        routes: [
          '/', 
          '/contato', 
          '/produtos/cms', 
          '/produtos/multicast', 
          '/produtos/workflow',
          '/solucoes',
          '/recursos',
          '/contato',
          '/demo'
        ],
      }),
    ],
  },

  pluginOptions: {
    i18n: {
      locale: 'pt',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
};
