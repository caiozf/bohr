import Vue from 'vue'
//import './registerServiceWorker'
import router from './router'
import VueMeta from 'vue-meta'
import i18n from './i18n'
import VueTheMask from 'vue-the-mask'
import Vuelidate from 'vuelidate'
import vueSmoothScroll from 'vue2-smooth-scroll'
import 'vue-slick-carousel/dist/vue-slick-carousel.css';

Vue.config.productionTip = false
Vue.use(VueMeta)
Vue.use(VueTheMask)
Vue.use(Vuelidate)
Vue.use(vueSmoothScroll)

import App from './App.vue';

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
