import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Error404 from '../views/Error404.vue'
import Products from '../views/products/Index.vue'
import Solutions from '../views/Solutions.vue'
import Contact from '../views/Contact.vue'
import Demo from '../views/Demo.vue'
import ResourcesIndex from '../views/resources/Index'
import ResourcesShow from '../views/resources/Show'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    name: 'ErrorPage',
    component: Error404
  },
  
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/produtos/:slug',
    name: 'Products',
    component: Products,
    props: true
  },

  {
    path: '/solucoes',
    name: 'Solutions',
    component: Solutions
  },
  
  {
    path: '/recursos',
    name: 'ResourcesIndex',
    component: ResourcesIndex
  },
  
  {
    path: '/recursos/:slug',
    name: 'ResourcesShow',
    component: ResourcesShow,
    props: true
  },

  {
    path: '/contato',
    name: 'Contact',
    component: Contact
  },

  {
    path: '/demo',
    name: 'Demo',
    component: Demo
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  window.scrollTo(0,0)

  next()
});

export default router
