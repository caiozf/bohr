let phoneMask = {
  computed: {
    phoneMask() {
      if(this.$i18n.locale === 'en') {
        return '(###) ###-####' 
      }

      return '(##)#####-####'
    }
  }
};

export default phoneMask;