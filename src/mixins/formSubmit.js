import axios from 'axios';

let headers = {
  'accept':'application/json',
  'content-type': 'application/json'
};

/*
  axios já sendo chamado acima
  variavel com os headers tambem
  Basta usar dentro do metodo "submitForm"
*/

let formSubmit = {
  data() {
    return {
      showFeedback: false,
      feedbackType: 'loading',
      amount: 0
    }
  },

  methods: {
    submitForm() {
      this.$v.$touch();

      console.log(this.form);
      /*
        this.form é o objeto que vem de cada formulario
        para ser usado no axios, se for necessário dê um JSON.parse
      */

      if(!this.$v.$error) {
        this.showFeedback = true;
        this.feedbackType = 'loading';

        //Simulando um submit abaixo

        let percent = setInterval(() => {
          this.amount++;

          if(this.amount === 100) {
            window.clearInterval(percent);
          }
        }, 25);

        setTimeout(() => {
          this.feedbackType = 'success';
        }, 2500);

        /*
          Para fazer o "amount" pegar o resultado real do server
          utilizar onUploadProgress do axios

          https://github.com/axios/axios#request-config
        */
      }

    }
  }
};

export default formSubmit;